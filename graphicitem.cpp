#include "graphicitem.h"

GraphicItem::GraphicItem(GraphicType _graphicType) :
    graphicType(_graphicType)
{
    QPainterPath path;
    switch (graphicType) {
        case Squad:
            itemPolygon << QPointF(-100, 0) << QPointF(0, 100)
                  << QPointF(100, 0) << QPointF(0, -100)
                  << QPointF(-100, 0);
            break;
        case Round:
            itemPolygon << QPointF(-100, -100) << QPointF(100, -100)
                      << QPointF(100, 100) << QPointF(-100, 100)
                      << QPointF(-100, -100);
            break;
    }
    setPolygon(itemPolygon);
    setFlag(QGraphicsItem::ItemIsMovable, true);
    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}
