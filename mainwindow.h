#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QHBoxLayout>
#include <QMenuBar>
#include <QMessageBox>
#include <QGraphicsView>
#include <QButtonGroup>
#include <QToolBox>
#include <QToolButton>
#include <QLabel>
#include <QDebug>

#include "graphicscene.h"

QT_BEGIN_NAMESPACE
class QAction;
class QToolBox;
class QSpinBox;
class QComboBox;
class QFontComboBox;
class QLineEdit;
class QGraphicsTextItem;
class QFont;
class QToolButton;
class QAbstractButton;
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void about();
    void buttonGroupClicked(int id);
    void itemInserted(GraphicItem *item);
    void itemSelected(QGraphicsItem *item);
private:
    void createToolBox();
    void createActions();
    void createMenus();
    void createToolbars();

    QWidget *createCellWidget(const QString &text, GraphicItem::GraphicType type);

    QMenu *fileMenu;
    QMenu *itemMenu;
    QMenu *aboutMenu;

    QAction *exitAction;
    QAction *deleteAction;
    QAction *aboutAction;

    QToolBox *toolBox;
    QButtonGroup *buttonGroup;

    QGraphicsView *view;
    GraphicScene *scene;

};

#endif // MAINWINDOW_H
