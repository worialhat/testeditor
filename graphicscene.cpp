#include "graphicscene.h"

GraphicScene::GraphicScene(QObject *parent) :
    QGraphicsScene(parent)
{
    mode = MoveItem;
}

void GraphicScene::setMode(Mode _mode)
{
    mode = _mode;
}

void GraphicScene::setItemType(GraphicItem::GraphicType _type)
{
    itemType = _type;
}

void GraphicScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
    if (mouseEvent->button() != Qt::LeftButton)
        return;

    GraphicItem *item;
    switch (mode) {
        case InsertItem:
            item = new GraphicItem(itemType);
//            item->setBrush(myItemColor);
            addItem(item);
            item->setPos(mouseEvent->scenePos());
            emit itemInserted(item);
            break;
    default:
        ;
    }
    QGraphicsScene::mousePressEvent(mouseEvent);
}

void GraphicScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
//    if (myMode == InsertLine && line != 0) {
//        QLineF newLine(line->line().p1(), mouseEvent->scenePos());
//        line->setLine(newLine);
    if (mode == MoveItem) {
        QGraphicsScene::mouseMoveEvent(mouseEvent);
    }
}

void GraphicScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
//    if (line != 0 && myMode == InsertLine) {
//        QList<QGraphicsItem *> startItems = items(line->line().p1());
//        if (startItems.count() && startItems.first() == line)
//            startItems.removeFirst();
//        QList<QGraphicsItem *> endItems = items(line->line().p2());
//        if (endItems.count() && endItems.first() == line)
//            endItems.removeFirst();

//        removeItem(line);
//        delete line;

//        if (startItems.count() > 0 && endItems.count() > 0 &&
//            startItems.first()->type() == DiagramItem::Type &&
//            endItems.first()->type() == DiagramItem::Type &&
//            startItems.first() != endItems.first()) {
//            DiagramItem *startItem = qgraphicsitem_cast<DiagramItem *>(startItems.first());
//            DiagramItem *endItem = qgraphicsitem_cast<DiagramItem *>(endItems.first());
//            Arrow *arrow = new Arrow(startItem, endItem);
//            arrow->setColor(myLineColor);
//            startItem->addArrow(arrow);
//            endItem->addArrow(arrow);
//            arrow->setZValue(-1000.0);
//            addItem(arrow);
//            arrow->updatePosition();
//        }
//    }
//    line = 0;
    QGraphicsScene::mouseReleaseEvent(mouseEvent);
}
