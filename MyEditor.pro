#-------------------------------------------------
#
# Project created by QtCreator 2017-02-23T18:36:58
#
#-------------------------------------------------

QT       += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MyEditor
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    graphicitem.cpp \
    graphicscene.cpp

HEADERS  += mainwindow.h \
    graphicitem.h \
    graphicscene.h

RESOURCES = editor.qrc
