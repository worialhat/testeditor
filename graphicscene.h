#ifndef GRAPHICSCENE_H
#define GRAPHICSCENE_H

#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>

#include <graphicitem.h>

class GraphicScene : public QGraphicsScene
{
    Q_OBJECT
public:
    enum Mode { InsertItem, MoveItem };

    GraphicScene(QObject *parent = 0);

public slots:
    void setMode(Mode _mode);
    void setItemType(GraphicItem::GraphicType _type);

signals:
    void itemInserted(GraphicItem *item);
    void itemSelected(QGraphicsItem *item);

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) Q_DECL_OVERRIDE;
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) Q_DECL_OVERRIDE;

private:
    GraphicItem::GraphicType itemType;
    Mode mode;
};

#endif // GRAPHICSCENE_H
