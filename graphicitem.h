#ifndef GRAPHICITEM_H
#define GRAPHICITEM_H

#include <QGraphicsPixmapItem>

QT_BEGIN_NAMESPACE
class QPolygonF;
QT_END_NAMESPACE

class GraphicItem : public QGraphicsPolygonItem
{
public:
    enum GraphicType { Squad, Round };

    GraphicItem(GraphicType _graphicType);

    QPolygonF getPolygon() const { return itemPolygon; }
    GraphicType getGraphicType() const { return graphicType; }

private:
    GraphicType graphicType;
    QPolygonF itemPolygon;

};

#endif // GRAPHICITEM_H
